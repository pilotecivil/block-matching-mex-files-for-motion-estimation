#include "mex.h"
#include "stdio.h"
#include <math.h>
#include <matrix.h>
#include <stdlib.h>

/*
* bm_classic.cpp : Block matching for one point
* Written by Antoine Courcelles, <courcelles.a@gmail.com>
*/


/* Return indice for a two dimensional matrix
 * I : indice (i,j), length of dimension 1
 * O : Indice linear
 */
int B(const int &i, const int &j, const int  &d1){
    return i + (j * d1);
}


/* Extract bloc of an image
 * I : im initial, bloc final, limit of the bloc, size Y bloc, size Y img 
 */
void extractBloc(const double* im, double* bloc, const int &x1, const int &x2, const int &y1, const int &y2, const int &sizeBY, const int &yDim){
    
    for(int y=y1; y<y2; y++){
       for(int x=x1; x<x2; x++){  
           
            // INDICE IMG
            int i=B(y,x,yDim);      
            // INDICE BLOC
            int j=B(y-y1, x-x1, sizeBY);
            
            bloc[j]=im[i]; 
       }
    }
}

/* Display Bloc (debug)
 * I : bloc, size bloc, 
 */
void displayBloc(const double* bloc, const int &sizeX, const int &sizeY){
    
    mexPrintf("\n");
    for(int y=0; y<sizeY; y++){
        for(int x=0; x<sizeX; x++){
            
            int i=B(y,x,sizeY);              
            mexPrintf("%.2f ",bloc[i]);
        }
         mexPrintf("\n");
    }
}

/* Mean pixels of the bloc
 * I : bloc, size Bloc
 * O :  double mean
 */
double meanBloc(const double* bloc, const int &sizeX, const int &sizeY){
    
    double sum=0, nbElmt;    
    nbElmt = sizeX * sizeY;

    for(int i=0; i<nbElmt; i++)
        sum+=bloc[i];   
    
    return sum/nbElmt;   
}


/* Compute NSSD between two blocs
 * I : bloc 1 and 2, size Bloc
 * O :  score NSSD
 */
double NSSD(const double* bloc1, const double* bloc2, const int &sizeX, const int &sizeY){
    
    double sumTop=0, sumB1=0, sumB2=0, diff1, diff2, mb1, mb2, nssd;    
    int nbElmt = sizeX * sizeY;
    
    // MEAN BLOCS
    mb1=meanBloc(bloc1, sizeX, sizeY);     
    mb2=meanBloc(bloc2, sizeX, sizeY);  
    
    for(int i=0; i<nbElmt; i++){
        
        // COMPUTE NUMERATOR
        diff1=bloc1[i]-mb1;
        diff2=bloc2[i]-mb2;
        
        sumTop+=pow((diff1-diff2), 2);
        
        // COMPUTE DENOMINATOR
        sumB1+=pow(bloc1[i]-mb1,2);
        sumB2+=pow(bloc2[i]-mb2,2); 
    }
    
    return sumTop/sqrt(sumB1*sumB2);
}


/* MEX FONCTION : Manage input and output, run BM for each bloc
 * I : image for frame 1 and 2, position of point of interest, margin for the block
 *      width of the windows for the research
 * O : map : matrix with all BM Score the block
 */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
 
    mxArray *img1, *img2, *bloc1, *bloc2, *my_map;
    const mwSize *dims;
    int px, py, mx, my, h_win_x_1, h_win_x_2, h_win_y_1, h_win_y_2, xDim, yDim;
    int sizeBX, sizeBY;
    double *im1, *im2,*b1, *b2, *map;    
    int fx_1, fx_2, fy_1, fy_2,fx, fy;
  
    // Recup Input VAR
    
    img1 = mxDuplicateArray(prhs[0]);
    img2 = mxDuplicateArray(prhs[1]);

	px = (int)mxGetScalar(prhs[2])-1; // indice
	py = (int)mxGetScalar(prhs[3])-1; // indice
    
    mx = (int)mxGetScalar(prhs[4]); 
    my = (int)mxGetScalar(prhs[5]); 
    
    h_win_x_1 = (int)mxGetScalar(prhs[6]); 
    h_win_x_2 = (int)mxGetScalar(prhs[7]); 
    h_win_y_1 = (int)mxGetScalar(prhs[8]);     
    h_win_y_2 = (int)mxGetScalar(prhs[9]); 
    
    
    // DIM IMG
    dims = mxGetDimensions(prhs[0]);
	yDim = (int)dims[0];
	xDim = (int)dims[1];
    
    // GET POINTER
    im1= mxGetPr(img1);
    im2= mxGetPr(img2);
    
    // Compute size Bloc
    sizeBX=2*mx+1;
    sizeBY=2*my+1;

    // INIT BLOC    
    bloc1 = mxCreateDoubleMatrix(sizeBY, sizeBX, mxREAL);
    b1 = mxGetPr(bloc1);
    
    bloc2 = mxCreateDoubleMatrix(sizeBY, sizeBX, mxREAL);
    b2 = mxGetPr(bloc2);
    
    // OUTPUT MATRICE
    fx=h_win_x_1+h_win_x_2+1;
    fy=h_win_y_1+h_win_y_2+1;
    my_map = plhs[0] = mxCreateDoubleMatrix(fy, fx, mxREAL);
    map = mxGetPr(my_map);
    
    // WINDOWS SEARCH
    fx_1=px-h_win_x_1+1;
    fx_2=px+h_win_x_2+2;
    
    fy_1=py-h_win_y_1+1;
    fy_2=py+h_win_y_2+2;
    
    // Bloc Ref
	extractBloc(im1, b1, px - mx, px + mx + 1, py - my, py + my + 1, sizeBY, yDim);


    for(int x=fx_1; x<fx_2; x++){
        for(int y=fy_1; y<fy_2; y++){
            
            // Bloc IMG 2
            extractBloc(im2, b2, x - mx - 1, x + mx, y - my - 1, y + my, sizeBY, yDim);
            
            int i = B(y - py + h_win_y_1-1, x - px + h_win_x_1-1, fy);

            map[i]=NSSD(b1, b2, sizeBX, sizeBY);
        }
    }   
}




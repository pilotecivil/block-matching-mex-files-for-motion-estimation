% example.m
% Written by Antoine Courcelles, <courcelles.a@gmail.com>


% Write by Antoine Courcelles, <courcelles.a@gmail.com>


% Your image has to be three dimensional (WIDTH, DEPTH, NB_FRAMES)
img = load('yourimage');
s=size(img);
NB_FRAMES = s(3);


% Windows of research, not necessarily symmetric
% the point will be move in this range for the search
H_WIN_X=[10, 10]; 
H_WIN_Y=[5, 5];

% % margin a block : size block = 2*mx +1 and 2*my +1
mx = 20;
my = 20;


%% ------------------------------------------------------------------------------
% One block
% ------------------------------------------------------------------------------

% coordinates of the point of interest
px = 100;
py = 100;


% block matching between image 1 and 2 for a block center on (px, py) avec a margin (mx, my)

my_map = bm_classic(img(:,:,1), img(:,:,2), px, py, mx, my, H_WIN_X(1), H_WIN_X(2), H_WIN_Y(1), H_WIN_Y(2));


% Search the minimal score and the associated motion 
[~, my_min] = min(my_map(:)); % score minimal
[mov_y, mov_x] = ind2sub(size(my_map), my_min); % indice X and Y for this score
mov_x = mov_x - (H_WIN_X(1) + 1); % motion relative to the point of interest
mov_y = mov_y - (H_WIN_Y(1) + 1); 

mov_x and mov_y are the displacement of (px, py) between the two frames


%% ------------------------------------------------------------------------------
% Several blocks
% ------------------------------------------------------------------------------
NB_POINT = 2;
size_x=sum(H_WIN_X)+1;
size_y=sum(H_WIN_Y)+1;

% matrix for the scores of BM
v_map=zeros(NB_POINT,size_y, size_x);

% matrix point 
v_point=zeros(NB_POINT, NB_FRAMES, 2);  

% initialisation point 
v_point(1, 1, :)= [100, 100]; 
v_point(2, 1, :)= [500, 200];

% Block matching for all the frames and for all the points

for i=2:NB_FRAMES
    disp(['Frame' int2str(i)]);

    % squeeze : send to the function only the matrix for the position of the points at one moment
    vpt = squeeze(v_point(:,i-1,:));
	v_map=bm_mutliple(img(:,:,1), img(:,:,2), mx, my, H_WIN_X(1), H_WIN_X(2), H_WIN_Y(1), H_WIN_Y(2), vpt, v_map);

	for j=1:NB_POINT

		% Search the minimal score and the associated motion 
        map = v_map(j,:,:);
		[~, my_min] = min(map(:)); 
		[~, mov_y, mov_x] = ind2sub(size(map), my_min);
		mov_x = mov_x - (H_WIN_X(1) + 1); 
		mov_y = mov_y - (H_WIN_Y(1) + 1);
 
		v_point(j, i, 1) = v_point(j, i-1, 1) + mov_x;	
		v_point(j, i, 2) = v_point(j, i-1, 2) + mov_y;	
	end
end

% Display point

color=char('r  ','g  ','b  ','c  ');
figure(1);
hold on;
for j=1:NB_POINT
	plot(v_point(j, :, 1), color(j,:));
end
title('X Point');
hold off;

figure(2);
hold on;
for j=1:NB_POINT
	plot(v_point(j, :, 2), color(j,:));
end
title('X Point');
hold off;










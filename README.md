# Block Matching Mex-files for Motion Estimation

This programm implements a block matching function in MEX/C++. The NSSD method is used for the compute of the score. The best matching correspond then at the minimal score.

Refer you at the file example.m

Tested on Ubuntu 14.04 LTS, 64 bits. Matlab R2012a


## One block

If you want to follow only one point, use bm_classic. 
You have to send the two images, the coordinates of the point of interest, the size of your block, the margin and the size of the windows for the search.
Please, check the file blockandwindows.png for a better explanation.

## Severals blocks

If you want to follow several points, use bm_multiple.
You have to send the two images, the size of yours blocks (the same for all), the margin and the size of the windows for the search. Send also the matrix with your initial positions for your points and the matrix v_map for the score.
Please, check the file blockandwindows.png for a better explanation.


## The matrix map (return of the BM function)

The mex fonction return you a matrix with all the scores of Block matching according to the different positions.
The indice (x, y) of the matrix correspond at the motion (x - h_win_x_1) +1 and (y - h_win_y_1) +1.
In effect, the position of the point (px, py) in the matrix depends on the size of your windows (h_win_x_1 and h_win_y_1).

## Compilation

Befor use the matlab code, you have to compile the mex files.

    mex *.cpp

## Note

Please note that the matlab code was written in june 2014 when the author was an intern at the [BIGR](http://bigr.nl/website/) (Biomedical Group of Rotterdam).

I truly hope that this code is as much help to you as it was fun for me to write!
If you have any question or suggestion, please contact me.

This programm is a FREE SOFTWARE, please read the licence file.


Antoine Courcelles

Copyright © 23/06/2014, Antoine Courcelles, courcelles.a@gmail.com
#include "mex.h"
#include "stdio.h"
#include <math.h>
#include <matrix.h>
#include <stdlib.h>

/*
* bm_multiple.cpp : Block matching for several points
* Written by Antoine Courcelles, <courcelles.a@gmail.com>
*/



/* Return indice for a three dimensional matrix
 * I : indice (i,j), length of dimension 1
 * O : Indice linear
 */
int A(const int &i, const int &j, const int &k, const int  &d1, const int &d2){
    return i + (j * d1) + (k * d1 * d2);
}

/* Return indice for a two dimensional matrix
 * I : indice (i,j), length of dimension 1
 * O : Indice linear
 */
int B(const int &i, const int &j, const int  &d1){
    return i + (j * d1);
}


/* Extract bloc of an image
 * I : im initial, bloc final, limit of the bloc, size Y bloc, size Y img 
 */
void extractBloc(const double* im, double* bloc, const int &x1, const int &x2, const int &y1, const int &y2, const int &sizeBY, const int &yDim){
    
    for(int y=y1; y<y2; y++){
       for(int x=x1; x<x2; x++){  
           
            // INDICE IMG
            int i=B(y,x,yDim);      
            // INDICE BLOC
            int j=B(y-y1, x-x1, sizeBY);
            
            bloc[j]=im[i]; 
       }
    }
}

/* Display Bloc (debug)
 * I : bloc, size bloc, 
 */
void displayBloc(const double* bloc, const int &sizeX, const int &sizeY){
    
    mexPrintf("\n");
    for(int y=0; y<sizeY; y++){
        for(int x=0; x<sizeX; x++){
            
            int i=B(y,x,sizeY);              
            mexPrintf("%.2f ",bloc[i]);
        }
         mexPrintf("\n");
    }
}

/* Mean pixels of the bloc
 * I : bloc, size Bloc
 * O :  double mean
 */
double meanBloc(const double* bloc, const int &sizeX, const int &sizeY){
    
    double sum=0, nbElmt;    
    nbElmt = sizeX * sizeY;

    for(int i=0; i<nbElmt; i++)
        sum+=bloc[i];   
    
    return sum/nbElmt;   
}


/* Compute NSSD between two blocs
 * I : bloc 1 and 2, size Bloc
 * O :  score NSSD
 */
double NSSD(const double* bloc1, const double* bloc2, const int &sizeX, const int &sizeY){
    
    double sumTop=0, sumB1=0, sumB2=0, diff1, diff2, mb1, mb2, nssd;    
    int nbElmt = sizeX * sizeY;
    
    // MEAN BLOCS
    mb1=meanBloc(bloc1, sizeX, sizeY);     
    mb2=meanBloc(bloc2, sizeX, sizeY);  
    
    for(int i=0; i<nbElmt; i++){
        
        // COMPUTE NUMERATOR
        diff1=bloc1[i]-mb1;
        diff2=bloc2[i]-mb2;
        
        sumTop+=pow((diff1-diff2), 2);
        
        // COMPUTE DENOMINATOR
        sumB1+=pow(bloc1[i]-mb1,2);
        sumB2+=pow(bloc2[i]-mb2,2); 
    }
    
    return sumTop/sqrt(sumB1*sumB2);
}


/* BM Function
 * Extract blocs and compute NSSD for each position
 * I : a lot!
 * O : Fill the matrix
 */
void bm(const double *im1, const double *im2, const double &px, const double &py,\
        const int &mx, const int &my, const int &nb_pt,const int &NB_POINT,\
        const int &h_win_x_1, const int &h_win_x_2, const int &h_win_y_1,\
        const int &h_win_y_2, const int &fy, const int &yDim, double *v_map){
                  
    mxArray *bloc1, *bloc2;
    double *b1, *b2;
    int sizeBX, sizeBY, fx_1, fx_2, fy_1, fy_2;
    
    
    // COMPUTE SIZE BLOC
    sizeBX = 2 * mx + 1;
    sizeBY = 2 * my + 1;   
    

    // INIT BLOC    
    bloc1 = mxCreateDoubleMatrix(sizeBY, sizeBX, mxREAL);
    b1 = mxGetPr(bloc1);
    
    bloc2 = mxCreateDoubleMatrix(sizeBY, sizeBX, mxREAL);
    b2 = mxGetPr(bloc2);   
    
   
    // WINDOWS SEARCH
    fx_1 = px - h_win_x_1 + 1;
    fx_2 = px + h_win_x_2 + 2;
    
    fy_1 = py - h_win_y_1 + 1;
    fy_2 = py + h_win_y_2 + 2;
    
    
    // BLOC REF
    extractBloc(im1, b1, px - mx, px + mx + 1, py - my, py + my + 1, sizeBY, yDim);


    for(int x=fx_1; x<fx_2; x++){
        for(int y=fy_1; y<fy_2; y++){
                       
            // BLOC IMG2
            extractBloc(im2, b2, x - mx - 1, x + mx, y - my - 1, y + my, sizeBY, yDim);            
                        
            // INDICE MATRIX
            int i = y - py + h_win_y_1 - 1;
            int j = x - px + h_win_x_1 - 1;           
            
            // COMPUTE NSSD AND FILL THE MATRIX
            v_map[A(nb_pt, i, j, NB_POINT, fy)] = NSSD(b1, b2, sizeBX, sizeBY); 

        }
    }
}


/* MEX FONCTION : Manage input and output, run BM for each bloc
 * I : image for frame 1 and 2, matrix position point and map to fill with BM score
 *      size of : bloc, windows, margin, size
 * O : v_map : matrix with all BM Score for each bloc
 */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){

  	mxArray *img1, *img2, *vpoint, *vmap;
    const mwSize *dims, *dims_map;
    double *im1, *im2, *v_pt, *v_map; 

    int mx, my, h_win_x_1, h_win_x_2, h_win_y_1, h_win_y_2, xDim, yDim;
    int sizeBX, sizeBY, NB_POINT, fy, fx;
    int fx_1, fx_2, fy_1, fy_2;
    
  
    // INPUT VAR
    
    img1 = mxDuplicateArray(prhs[0]);
    img2 = mxDuplicateArray(prhs[1]);
  
    mx = (int)mxGetScalar(prhs[2]); 
    my = (int)mxGetScalar(prhs[3]); 
    
    h_win_x_1 = (int)mxGetScalar(prhs[4]); 
    h_win_x_2 = (int)mxGetScalar(prhs[5]); 
    h_win_y_1 = (int)mxGetScalar(prhs[6]);     
    h_win_y_2 = (int)mxGetScalar(prhs[7]); 
    
    vpoint = mxDuplicateArray(prhs[8]);
    vmap = mxDuplicateArray(prhs[9]);
    
    // OUTPUT VAR
    plhs[0] = vmap;
    
    // DIM IMG
    dims = mxGetDimensions(prhs[0]);
	yDim = (int)dims[0];
	xDim = (int)dims[1];
    
    // DIM v_map
    dims_map = mxGetDimensions(prhs[9]);
	NB_POINT = (int)dims_map[0];
	fy = (int)dims_map[1];
    fx = (int)dims_map[2];
       
    
    // GET POINTER
    im1= mxGetPr(img1);
    im2= mxGetPr(img2);
    v_pt= mxGetPr(vpoint);
    v_map= mxGetPr(vmap);

       
    // BM FOR EACH BLOC
    for(int k=0; k<NB_POINT; k++)        
        bm(im1, im2, v_pt[B(k,0, NB_POINT)], v_pt[B(k,1, NB_POINT)], mx, my, k, NB_POINT,\
                h_win_x_1, h_win_x_2, h_win_y_1, h_win_y_2, fy, yDim, v_map);
       

}

